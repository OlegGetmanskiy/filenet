package org.ogetman.filenet;

import com.ibm.icu.text.Transliterator;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.NoRouteToHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

/**
 *
 * @author OlegusGetman
 */
class Tasks {
    public final static String ADDR = "olegusgetman.ru";
    //public final static String ADDR = "localhost";
    public final static String LOGIN = "ftpuser";
    public final static String PW = "klodrox";
    
    public static String currentLogin = null;
    public static int ownUserRow = -1;
    private static volatile boolean busy = false;
    private static final List<String> reservedWords = new ArrayList();
    
    static {
        reservedWords.add("xferstat.dat");
        reservedWords.add("motd.txt");
        reservedWords.add("anonymous");
    }
    
    public static void doLogin() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(busy) return;
                busy = true;
                MainFrame.BlockUI();
                try {
                    MainFrame.lStatus.setText("Подключение...");
                    FTPClient ftp = getFtp();
                    //ftp.connect(ADDR);
                    /*if(!ftp.login(LOGIN, PW)) {
                        throw new IOException();
                    }*/
                    String login = MainFrame.tfLogin.getText();
                    String password = MainFrame.tfPassword.getText();
                    ftp.changeWorkingDirectory(login);
                    
                    //test
                    /*try (InputStream testStream = ftp.retrieveFileStream(".pw")) {
                        System.out.println(testStream == null); 
                        System.out.println(ftp.getReplyString());
                    }
                    ftp.completePendingCommand();*/
                    //end of test
                    
                    ftp.enterLocalPassiveMode();
                    Scanner scanner = new Scanner(ftp.retrieveFileStream(".pw"));
                    if(scanner.nextLine().equals(password)) {
                        scanner.close();
                        ftp.completePendingCommand();
                        currentLogin = login;
                        fillUsersList();
                        MainFrame.listUsers.setSelectedIndex(ownUserRow);
                        MainFrame.loginPanel.setVisible(false);
                        doFillFilesList(currentLogin);
                        MainFrame.lStatus.setText("Авторизация выполнена");
                    } else {
                        scanner.close();
                        ftp.completePendingCommand();
                        MainFrame.lStatus.setText("Неверная авторизация");
                    }
                } catch (IOException ex) {
                    MainFrame.lStatus.setText("Ошибка подключения к серверу");
                    JOptionPane.showMessageDialog(null, ex.toString());
                } catch (NullPointerException ex) {
                    //JOptionPane.showMessageDialog(null, ex.toString());
                    MainFrame.lStatus.setText("Неверная авторизация");
                } finally {
                    busy = false;
                    MainFrame.UnblockUI();
                }
            }
        });
        t.start();
    }
    
    public static void doRegister() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(busy) return;
                busy = true;
                MainFrame.BlockUI();
                PrintWriter writer = null;
                try {
                    String username = MainFrame.tfLogin.getText();
                    String password = MainFrame.tfPassword.getText();
                    if(!validLogin(username)) {
                        MainFrame.lStatus.setText("Некорректный логин (должен содержать только латинские буквы)");
                        return;
                    }
                    if(reservedWords.contains(username.toLowerCase())) {
                        MainFrame.lStatus.setText("Недопустимый логин");
                        return;
                    }
                    if(!validPassword(password)) {
                        MainFrame.lStatus.setText("Слишком котроткий пароль (минимум 6 символов)");
                        return;
                    }
                    MainFrame.lStatus.setText("Отправка данных...");
                    if(authExists(username)) {
                        MainFrame.lStatus.setText("Такой пользователь уже существует!");
                        return;
                    }   
                    FTPClient f = getFtp();
                    f.makeDirectory("/"+username);
                    f.changeWorkingDirectory(username);
                    writer = new PrintWriter(f.storeFileStream(".pw"));
                    writer.write(password);
                    writer.close();
                    f.completePendingCommand();
                    //f.disconnect();
                    MainFrame.lStatus.setText("Успешно зарегистрирован");
                } catch (IOException ex) {
                    Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
                    MainFrame.lStatus.setText(ex.getMessage());
                } finally {
                    //writer.close();
                    busy = false;
                    MainFrame.UnblockUI();
                }                    
            }
        });
        t.start();
    }

    public static boolean authExists(String user) {
        FTPClient f = getFtp();
        try {
            f.changeToParentDirectory();
            for(FTPFile file : f.listFiles("")) {
                if(file.getName().equalsIgnoreCase(user)) {
                    //f.disconnect();
                    return true;
                }
            }
            return false;
        } catch (IOException ex) {
            /*try {
                //f.disconnect();
            } catch (IOException ex1) {
                Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex1);
            }*/
            return false;
        }
    }

    public static void doFillUsersList() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if (busy) return;
                busy = true;
                MainFrame.BlockUI();
                fillUsersList();
                busy = false;
                MainFrame.UnblockUI();
            }
        });
        t.start();
    }
    
    public static void fillUsersList() {
        try {
            FTPClient ftp = getFtp();
            ftp.changeToParentDirectory();
            FTPFile[] files = ftp.listFiles("");
            DefaultListModel listModel = new DefaultListModel();
            int cnt = 0;
            for(FTPFile file : files) {
                if(!file.getName().startsWith(".") && !reservedWords.contains(file.getName())) {
                    String user = file.getName();
                    listModel.addElement(user);
                    if(user.equals(currentLogin)) {
                        ownUserRow = cnt;
                    }
                    cnt++;
                }
            }
            MainFrame.listUsers.setModel(listModel);
        } catch (IOException ex) {
            Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void doFillFilesList(final String user) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient ftp = getFtp();
                try {
                    if(busy) return;
                    busy = true;
                    MainFrame.BlockUI();
                    //MainFrame.tFiles.setRowSelectionInterval(0, 0);
                    ftp.changeToParentDirectory();
                    ftp.changeWorkingDirectory(user);
                    FTPFile[] files = ftp.listFiles("");
                    DefaultTableModel listModel = (DefaultTableModel) MainFrame.tFiles.getModel();
                    MainFrame.tFiles.clearSelection();
                    listModel.getDataVector().removeAllElements();
                    listModel.setRowCount(files.length+100);
                    int cnt = 0;
                    for (FTPFile file : files) {
                        if (!file.getName().startsWith(".") && !reservedWords.contains(file.getName())) {
                            listModel.setValueAt(file.getName(), cnt, 0);
                            listModel.setValueAt(String.format("%.2f MB",(float)file.getSize() / (float)1024 / (float)1024), cnt, 1);
                            cnt++;
                        }
                    }
                    listModel.setRowCount(cnt);
                } catch (NoRouteToHostException ex) {
                    MainFrame.lStatus.setText("Связь с сервером потеряна");
                } catch (IOException ex) {
                    Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    busy = false;
                    /*try {
                        ftp.disconnect();
                    } catch (IOException ex) {
                        //Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
                    }*/
                    MainFrame.UnblockUI();
                }
            }
        });
        t.start();
    }
    
    public static void doDownload() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                File localFile;
                String user = (String) MainFrame.listUsers.getSelectedValue();
                String file = (String) MainFrame.tFiles.getValueAt(MainFrame.tFiles.getSelectedRow(), 0);
                if ((localFile = confirmSave(file)) != null) {
                    if (busy) {
                        return;
                    }
                    busy = true;
                    MainFrame.BlockUI();
                    FTPClient ftp = getFtp();
                    InputStream in;
                    //ftp.setBufferSize(1024*1024);
                    try {
                        ftp.changeToParentDirectory();
                        ftp.setFileType(FTP.BINARY_FILE_TYPE);
                        MainFrame.lStatus.setText("Скачиваем файл...");
                        OutputStream out = new FileOutputStream(localFile);
                        in = ftp.retrieveFileStream(user+"/"+file);
                        
                        String sizeStr = (String)MainFrame.tFiles.getValueAt(MainFrame.tFiles.getSelectedRow(),1);
                        sizeStr = sizeStr.substring(0, sizeStr.length()-3);
                        sizeStr = sizeStr.replace(',', '.');
                        int size = (int)(Float.valueOf(sizeStr) * 1024 * 1024);
                        //ftp.completePendingCommand();
                        
                        MainFrame.pbProgress.setIndeterminate(false);
                        MainFrame.pbProgress.setMaximum((int)size);
                        MainFrame.pbProgress.setValue(0);
                        
                        int total = 0;
                        int read = 0;
                        byte[] bytes = new byte[1024];
                        //MainFrame.bCancelTransfer.setVisible(true);
                        while ((read = in.read(bytes)) != -1) {
                            out.write(bytes, 0, read);
                            total+=read;
                            MainFrame.pbProgress.setValue(total);
                            MainFrame.lStatus.setText("Скачиваем " + file + " (" + asMegaBytes(total) + " MB / " + asMegaBytes(size) + " MB)");
                        }
                        
                        in.close();
                        out.flush();
                        out.close();
                        ftp.completePendingCommand();
                        //ftp.disconnect();
                        MainFrame.lStatus.setText("Файл успешно скачан");
                        //MainFrame.bCancelTransfer.setVisible(false);
                        MainFrame.pbProgress.setValue(MainFrame.pbProgress.getMaximum());
                    } catch (IOException ex) {
                        MainFrame.lStatus.setText("Ошибка при передаче данных (" + ex.getMessage() + ")");
                        localFile.delete();
                    } catch (NullPointerException ex) {
                        MainFrame.lStatus.setText("Ошибка при передаче данных (возможно файл удален)");
                        localFile.delete();
                    } finally {
                        busy = false;
                        MainFrame.UnblockUI();
                    }
                }
            } 
        });
        t.start();
    }
    
    public static void doUpload() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                File localFile;
                if((localFile = confirmOpen()) != null) {
                    if(localFile.length() > 1024*1024*1024) {
                        MainFrame.lStatus.setText("Этот файл слишком большой (ограничение 1024 MB)");
                        MainFrame.UnblockUI();
                        return;
                    }
                    if(!localFile.exists()) {
                        MainFrame.lStatus.setText("Файл на компьютере не найден");
                        MainFrame.UnblockUI();
                        return;
                    }
                    if(busy) return;
                    busy = true;
                    MainFrame.BlockUI();
                    String user = currentLogin;
                    FTPClient ftp = getFtp();
                    try {
                        MainFrame.lStatus.setText("Загрузка файла на сервер...");
                        ftp.changeToParentDirectory();
                        ftp.changeWorkingDirectory(user);
                        ftp.setFileType(FTP.BINARY_FILE_TYPE);
                        OutputStream out = ftp.storeFileStream(filenameString(localFile.getName()));
                        InputStream in = new FileInputStream(localFile);
                        
                        int size = (int)localFile.length();
                        MainFrame.pbProgress.setIndeterminate(false);
                        MainFrame.pbProgress.setMaximum((int)size);
                        MainFrame.pbProgress.setValue(0);
                        
                        String localFileName = localFile.getName();
                        //MainFrame.bCancelTransfer.setVisible(true);
                        int total = 0;
                        int read = 0;
                        byte[] bytes = new byte[1024];
                        while ((read = in.read(bytes)) != -1) {
                            out.write(bytes, 0, read);
                            total+=read;
                            MainFrame.pbProgress.setValue(total);
                            MainFrame.lStatus.setText("Загружаем " + localFileName +  " (" + asMegaBytes(total) + " MB / " + asMegaBytes(size) + " MB)");
                        }
                        out.flush();
                        out.close();
                        in.close();
                        ftp.completePendingCommand();
                        MainFrame.pbProgress.setValue(MainFrame.pbProgress.getMaximum());
                        MainFrame.lStatus.setText("Файл успешно загружен");
                        //MainFrame.bCancelTransfer.setVisible(false);
                        doFillFilesList(currentLogin);
                        MainFrame.listUsers.setSelectedIndex(ownUserRow);
                    } catch(FileNotFoundException ex) {
                        MainFrame.lStatus.setText("Файл на компьютере не найден");
                    } catch (IOException ex) {
                        MainFrame.lStatus.setText("Ошибка при передаче данных");
                    } finally {
                        busy = false;
                        MainFrame.UnblockUI();
                    }
                }
                
            }
        });
        t.start();
    }
    
    public static void doDelete() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(busy) return;
                busy = true;
                String file = (String) (MainFrame.tFiles.getValueAt(MainFrame.tFiles.getSelectedRow(),0));
                FTPClient ftp = getFtp();
                try {
                    ftp.changeToParentDirectory();
                    ftp.changeWorkingDirectory(currentLogin);
                    if(ftp.deleteFile(file)) {
                        MainFrame.lStatus.setText("Файл " + file + " удален");
                        doFillFilesList(currentLogin);
                    }
                    busy = false;
                    MainFrame.UnblockUI();
                } catch (IOException ex) {
                    Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        t.start();
    }

    public static void doEstablishConnection() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                MainFrame.BlockUI();
                try {
                    if(ftpClient.isConnected()) ftpClient.disconnect();
                    long startTime = System.currentTimeMillis();
                    ftpClient.connect(ADDR,21);
                    long pingTime = System.currentTimeMillis() - startTime;
                    ftpClient.login(LOGIN, PW);
                    ftpClient.setFileType(FTP.ASCII_FILE_TYPE);
                    try (Scanner scanner = new Scanner(ftpClient.retrieveFileStream("motd.txt"),"Cp1251")) {
                        StringBuilder motd = new StringBuilder("<html>");
                        while(scanner.hasNextLine())
                            motd.append(scanner.nextLine()).append("<br>");
                        MainFrame.lMotd.setText(motd.toString() + "(Ответ за " + pingTime + " мс)");
                    }
                    ftpClient.completePendingCommand();
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    MainFrame.lStatus.setText("Соединение с сервером установлено");
                } catch (IOException ex) {
                    MainFrame.lStatus.setText("Ошибка подключения (" + ex.getMessage() + ")");
                } finally {
                    MainFrame.UnblockUI();
                }
            }
        });
        t.start();
    }
    
    public static void doCancelTransfer() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(ftpClient.abort()) {
                        MainFrame.lStatus.setText("Операция отменена");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        t.start();
    }
    
    private static File confirmSave(String serverFileName) {
        JFileChooser chooser = new JFileChooser();
        chooser.setPreferredSize(new Dimension(800,600));
        chooser.setSelectedFile(new File(serverFileName));
        if(chooser.showSaveDialog(null) == APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }
        return null;
    }
    
    private static File confirmOpen() {
        JFileChooser chooser = new JFileChooser();
        chooser.setPreferredSize(new Dimension(800,600));
        if(chooser.showOpenDialog(null) == APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }
        return null;
    }
    
    private static final FTPClient ftpClient = new FTPClient();

    private static FTPClient getFtp() {
        try {
            if(!ftpClient.isConnected()) {
                ftpClient.rein();
            }
        } catch (IOException ex) {
            Logger.getLogger(Tasks.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ftpClient;
    }
    
    private static String filenameString(String str) {
        String string = str.replace(' ','_');
        if(string.startsWith(".")) {
            string = string.substring(1);
        }
        String id = "Any-Latin; NFD; [^\\p{Alnum}.] Remove";
        String latin = Transliterator.getInstance(id).transform(string);
        return latin;
    }
    
    private static String asMegaBytes(int n) {
        return String.format("%.2f", (float)n/1024/1024);
    }
    
    private static boolean validLogin(String login) {
        return login.matches("\\w+");
    }
    
    private static boolean validPassword(String pw) {
        return pw.length() > 5;
    }
}